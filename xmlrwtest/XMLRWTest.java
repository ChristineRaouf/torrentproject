/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlrwtest;

import folder.Folder;
import java.util.Iterator;
import torrents.Torrents;
import torrents.TorrentsInt;
import javax.tools.StandardLocation;
import tFile.TFile;

/**
 *
 * @author omnia
 */
public class XMLRWTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Torrents torrents = new Torrents("./src/summaryFile/summaryFile.xml");
        Folder fol = torrents.getCompleteTorrents().iterator().next();
        TFile f;
        for(Iterator it = fol.getFiles().iterator(); it.hasNext();) {
            f = (TFile)it.next();
            System.out.println("folder name: "+f.getName());
            System.out.println("folder status: "+f.getStatus());
            System.out.println("folder percentage: "+f.getPercentage());
        }
        fol  = new Folder();
        fol.setLocation("here");
        fol.setName("name");
        fol.setPercetage("3");
        fol.setStatus("notYet");
        torrents.addRecord(fol);
    } 
}
   
