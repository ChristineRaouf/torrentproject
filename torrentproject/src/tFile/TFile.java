/*
 * Copyright 2015 omnia.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tFile;

/**
 *
 * @author omnia
 */
public class TFile {
    protected String name;
    protected String status;
    protected String percentage;
    
    public void setName(String n) {
        name = n;
    }
    public void setStatus(String s) {
        status = s;
    }
    public void setPercetage(String p) {
        percentage = p;
    }
    
    public String getPercentage() {
        return percentage;
    }
    public String getName() {
        return name;
    }
    public String getStatus() {
        return status;
    }
    
}
