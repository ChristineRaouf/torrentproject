/*
 * Copyright 2015 omnia.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package folder;

import java.util.LinkedList;
import java.util.List;
import tFile.TFile;

/**
 *
 * @author omnia
 */
public class Folder {
    protected String name;
    protected String status;
    protected String location;
    protected String percentage;
    protected List <TFile> files = new LinkedList<TFile>();
    
    public void setName(String n) {
        name = n;
    }
    public void setStatus(String s) {
        status = s;
    }
    public void setLocation(String l) {
        location = l;
    }
    public void setPercetage(String p) {
        percentage = p;
    }
    public String getName() {
        return name;
    }
     public String getStatus() {
        return status;
    } public String getLocation() {
        return location;
    } public String getPercentage() {
        return percentage;
    }
    public void addFile(TFile file) {
        if(file != null)
        {
            files.add(file);
        }
    }
    public void removeFile(TFile file) {
        if(file != null)
        {
            files.remove(file);
        }
    }
    public Iterable getFiles() {
        return files;
    }
}
