/*
 * Copyright 2015 omnia.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package summaryFile;

import folder.Folder;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import tFile.TFile;

/**
 *
 * @author omnia
 */
public class SummaryFileImp implements SummaryFileInt{
    
    String path;

    public SummaryFileImp(String p) {
        path = p;
    }
    
    @Override
    public void addRecord(Folder f) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
       // dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new FileInputStream(new File(path)));
        Element element = doc.getDocumentElement();

        Element folder = doc.createElement("folder");
//        Node fName = doc.createAttribute("name");
//        fName.setNodeValue(f.getName());
//        Node fLoc = doc.createAttribute("location");
//        fLoc.setNodeValue(f.getLocation());
//        Node fStatus = doc.createAttribute("status");
//        fStatus.setNodeValue(f.getStatus());
//        Node fPercent = doc.createAttribute("percentage");
//        fPercent.setNodeValue(f.getPercentage());
      
        
//        folder.appendChild(fName);
//        folder.appendChild(fLoc);
//        folder.appendChild(fStatus);
          folder.setAttribute("percentage", f.getPercentage());
          folder.setAttribute("name", f.getName());
          folder.setAttribute("location", f.getLocation());
          folder.setAttribute("status", f.getStatus());
        
        for (Iterator it = f.getFiles().iterator(); it.hasNext();) {
            TFile tFile = (TFile) it.next();
            
            Element file = doc.createElement("file");
//            fName = doc.createAttribute("name");
//            fName.setNodeValue(fFile.getName());
//            fStatus = doc.createAttribute("status");
//            fStatus.setNodeValue(fFile.getStatus());
//            fPercent = doc.createAttribute("percentage");
//            fPercent.setNodeValue(fFile.getPercentage());
//            
//            file.appendChild(fPercent);
//            file.appendChild(fName);
//            file.appendChild(fLoc);
//            file.appendChild(fStatus);
            file.setAttribute("percentage", tFile.getPercentage());
            file.setAttribute("name", tFile.getName());;
            file.setAttribute("status", tFile.getStatus());
           
            folder.appendChild(file);
        }
        
       // System.out.println(element.getElementsByTagName("folder").item(0));
        element.insertBefore(folder, element.getFirstChild());
        element.normalize();
        
        // Format the xml for output
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        // initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(path);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        
    }

    @Override
    public void removeRecord(String fileName) {
        
    }
    
}
