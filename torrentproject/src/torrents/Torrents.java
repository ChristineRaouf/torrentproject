/*
 * Copyright 2015 omnia.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package torrents;

import folder.Folder;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tFile.TFile;
import javax.tools.StandardLocation;


/**
 *
 * @author omnia
 */
public class Torrents extends summaryFile.SummaryFileImp implements TorrentsInt{
    static LinkedList<Folder> torrents;
    
    public Torrents(String path)throws Exception {
        super(path);
         DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();

        //Get the DOM Builder
        DocumentBuilder builder = factory.newDocumentBuilder();
    //Load and Parse the XML document

        //document contains the complete XML as a Tree.
       // System.out.println(StandardLocation.locationFor(path));
        Document document
        /*        = builder.parse(
                        ClassLoader.getSystemResourceAsStream(path));*/
                  = builder.parse(new FileInputStream(path));
        torrents = new LinkedList<Folder>();

    //Iterating through the nodes and extracting the data.
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {

      //We have encountered an <folder> tag.
            Node node = nodeList.item(i);

            if (node instanceof Element) {

                Folder folder = new Folder();

                folder.setName(node.getAttributes().
                        getNamedItem("name").getNodeValue());
                folder.setLocation(node.getAttributes().
                        getNamedItem("location").getNodeValue());
                folder.setPercetage(node.getAttributes().
                        getNamedItem("percentage").getNodeValue());
                folder.setStatus(node.getAttributes().
                        getNamedItem("status").getNodeValue());

                NodeList childNodes = node.getChildNodes();

                for (int j = 0; j < childNodes.getLength(); j++) {

                    Node cNode = childNodes.item(j);
                    TFile  file;
                    //Identifying the child tag of file encountered.
                    if (cNode instanceof Element) {
                        file = new TFile();

                        file.setName(cNode.getAttributes().
                                getNamedItem("name").getNodeValue());
                        file.setPercetage(cNode.getAttributes().
                                getNamedItem("percentage").getNodeValue());
                        file.setStatus(cNode.getAttributes().
                                getNamedItem("status").getNodeValue());
                        folder.addFile(file);
                        System.out.println(file.getName());
                    }
                  
                }
                torrents.add(folder);
            }

        }
    }
    public List getTorrents() {
        return torrents;
    }
    private List getTorrentsThat(String status) {
        LinkedList<Folder> folders = new LinkedList<Folder>();
         for (Folder folder : torrents) {
             if(folder.getStatus().equals(status)) {
                 folders.add(folder);
             }
         }
        return folders;
    }
    public Iterable<Folder> getCompleteTorrents(){
        List<Folder> folders = getTorrentsThat("completed");
        return folders;
    }
    public Iterable<Folder> getStopTorrents(){
        List<Folder> folders = getTorrentsThat("stopped");
        return folders;
    }
    public Iterable<Folder> getDownloadTorrents(){
        List<Folder> folders = getTorrentsThat("download");
        return folders;
    }
}
